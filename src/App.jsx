import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import FullList from "./components/FullList";
import SearchItems from "./components/SearchItems";
import SearchedList from "./components/SearchedList";
function App() {
	return (
		<>
			<BrowserRouter>
				<SearchItems />
				<Routes>
					<Route path="/" exact element={<FullList />}></Route>
					<Route
						path="/search/:searchTerm"
						element={<SearchedList />}
					/>
				</Routes>
			</BrowserRouter>
		</>
	);
}

export default App;
