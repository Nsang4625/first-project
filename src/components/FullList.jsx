import React, { useState, useEffect } from "react";
import { Button } from "@chakra-ui/react";
import axios from "axios";
import List from "./List";
import Form from "./utils/Form";
import {
	Modal,
	ModalOverlay,
	ModalContent,
	ModalHeader,
	ModalBody,
	ModalCloseButton,
	ModalFooter,
} from "@chakra-ui/react";
import { useDisclosure } from "@chakra-ui/react";
import Student from "./utils/Student";

const FullList = () => {
	const [students, setStudents] = useState([]);
	const { isOpen, onClose, onOpen } = useDisclosure();
	const [neededLoad, setNeededLoad] = useState(true);
	const student = new Student("", "", "", "");
	useEffect(() => {
		try {
			const fetchingStudents = async () => {
				const res = await axios.get("http://localhost:4000/students");
				setStudents(res.data);
			};
			fetchingStudents();
		} catch (error) {
			console.log(error.message);
		}
	}, [neededLoad]);

	return (
		<div>
			<div className="mb-1 flex flex-row-reverse">
				<Button
					size="sm"
					colorScheme="teal"
					className="w-8 h-8"
					onClick={onOpen}
				>
					+
				</Button>
				<Modal isOpen={isOpen} onClose={onClose}>
					<ModalOverlay />
					<ModalContent>
						<ModalHeader>
							<ModalCloseButton />
						</ModalHeader>
						<ModalBody>
							<Form
								onClose={onClose}
								setNeededLoad={setNeededLoad}
								neededLoad={neededLoad}
								student={student}
								method="create"
							/>
						</ModalBody>
						<ModalFooter>
							<Button type="submit" form="newStudent">
								Lưu
							</Button>
						</ModalFooter>
					</ModalContent>
				</Modal>
			</div>
			<List
				students={students}
				setNeededLoad={setNeededLoad}
				neededLoad={neededLoad}
			/>
		</div>
	);
};

export default FullList;
