import ModalEdit from "./utils/ModalEdit";
const List = ({ students, setNeededLoad, neededLoad }) => {
	return (
		<div>
			<table className="min-w-full divide-y divide-gray-200">
				<thead className="bg-gray-50">
					<tr>
						<th
							scope="col"
							className="px-6 py-3 text-xs font-medium text-gray-500 uppercase tracking-wider"
						>
							Mã sv
						</th>
						<th
							scope="col"
							className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
						>
							Họ tên
						</th>
						<th
							scope="col"
							className="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider"
						>
							Giới tính
						</th>
						<th
							scope="col"
							className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
						>
							Tên lớp
						</th>
						<th scope="col" className="relative px-6 py-3">
							<span className="sr-only">Edit</span>
						</th>
					</tr>
				</thead>
				<tbody className="bg-white divide-y divide-gray-200">
					{students.map((student) => (
						<tr key={student.id}>
							<td className="px-6 py-4 whitespace-nowrap text-sm text-center text-gray-500">
								{student.id}
							</td>
							<td className="px-6 py-4  whitespace-nowrap text-sm text-center font-medium text-gray-900">
								{student.name}
							</td>
							<td className="px-6 py-4 whitespace-nowrap text-sm text-center text-gray-500">
								{student.gender}
							</td>
							<td className="px-6 py-4 whitespace-nowrap text-sm text-left text-gray-500">
								{student.className}
							</td>
							<td className="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
								<ModalEdit
									student={student}
									setNeededLoad={setNeededLoad}
									neededLoad={neededLoad}
								/>
							</td>
						</tr>
					))}
				</tbody>
			</table>
		</div>
	);
};

export default List;
