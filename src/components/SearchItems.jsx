import React, { useState } from "react";
import { SearchIcon } from "@chakra-ui/icons";
import { Button } from "@chakra-ui/button";
import { useNavigate } from "react-router";

const SearchItems = () => {
	const [searchTerm, setSeartchTerm] = useState("");
	const navigate = useNavigate();
	const handleSearch = () => {
		if (searchTerm) {
			navigate(`/search/${searchTerm}`);
		}
	};
	return (
		<div className="mb-4 flex">
			<input
				type="text"
				value={searchTerm}
				onChange={(e) => setSeartchTerm(e.target.value)}
				className="mr-2 bg-slate-200 text-black p-1 w-52 rounded-md"
				placeholder="Nhập tên sinh viên"
			/>
			<Button className="p-2 bg-slate-400" onClick={handleSearch}>
				<SearchIcon />
			</Button>
		</div>
	);
};

export default SearchItems;
