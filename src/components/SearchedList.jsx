import { useParams } from "react-router";
import { useState, useEffect } from "react";
import axios from "axios";
import List from "./List";
const SearchedList = () => {
	const [students, setStudents] = useState([]);
	const { searchTerm } = useParams();
	useEffect(() => {
		const fetchingStudents = async () => {
			const res = await axios.get("http://localhost:4000/students");
			const fetchedStudents = res.data;
			const filterdStudents = fetchedStudents.filter((student) => {
				return student.name.includes(searchTerm);
			});
			setStudents(filterdStudents);
		};
		fetchingStudents();
	}, []);
	return <List students={students} />;
};

export default SearchedList;
