import React from "react";
import { useState } from "react";
import {
	FormControl,
	FormLabel,
	FormErrorMessage,
	Input,
} from "@chakra-ui/react";
import axios from "axios";
import { useNavigate, useParams } from "react-router";
import Student from "./Student";

const Form = ({ onClose, setNeededLoad, neededLoad, student, method }) => {
	const [id, setId] = useState(student.id);
	const [name, setName] = useState(student.name);
	const [gender, setGender] = useState(student.gender);
	const [className, setClassName] = useState(student.className);

	const isValidId = /B[0-9]{2}DCCN[0-9]{3}/.test(id);
	const isValidClassName = /D[0-9]{2}CQCN[0-9]{2}/.test(className);
	const isValidGender = gender === "Nam" || gender === "Nữ";

	const [isIdError, setIsIdError] = useState(false);
	const [isClassNameError, setIsClassNameError] = useState(false);
	const [isGenderError, setIsGenderError] = useState(false);

	const { searchTerm } = useParams();
	const navigate = useNavigate();

	const handleSubmit = async (e) => {
		e.preventDefault();

		if (!isValidId || !isValidClassName || !isValidGender) {
			if (!isValidId) setIsIdError(true);
			if (!isValidClassName) setIsClassNameError(true);
			if (!isValidGender) setIsGenderError(true);
			return;
		}

		try {
			const config = {
				headers: {
					"Content-type": "application/json",
				},
			};
			const student = new Student(id, name, gender, className);
			if (method === "create") {
				await axios.post(
					"http://localhost:4000/students",
					student,
					config
				);
			} else if (method === "update") {
				await axios.put(
					`http://localhost:4000/students/${id}`,
					student,
					config
				);
				if (searchTerm) {
					navigate("/");
				}
			}
			onClose();
			setNeededLoad(!neededLoad);
		} catch (error) {
			console.log(error);
		}
	};
	return (
		<form id="newStudent" onSubmit={handleSubmit}>
			<FormControl isInvalid={isIdError} mb="8px">
				<FormLabel mb="-2px">MÃ SV</FormLabel>
				<Input
					onChange={(e) => setId(e.target.value)}
					placeholder="Nhập mã sinh viên"
					value={id}
				/>
				{isIdError && <FormErrorMessage>Không hợp lệ</FormErrorMessage>}
			</FormControl>
			<FormControl mb="8px">
				<FormLabel mb="-2px">HỌ TÊN</FormLabel>
				<Input
					onChange={(e) => setName(e.target.value)}
					placeholder="Nhập họ tên"
					value={name}
				/>
			</FormControl>
			<FormControl isInvalid={isGenderError} mb="8px">
				<FormLabel mb="-2px">GIỚI TÍNH</FormLabel>
				<Input
					onChange={(e) => setGender(e.target.value)}
					placeholder="Nhập giới tính"
					value={gender}
				/>
				{isGenderError && (
					<FormErrorMessage>Không hợp lệ</FormErrorMessage>
				)}
			</FormControl>
			<FormControl isInvalid={isClassNameError} mb="8px">
				<FormLabel mb="-2px">TÊN LỚP</FormLabel>
				<Input
					onChange={(e) => setClassName(e.target.value)}
					placeholder="Nhập tên lớp"
					value={className}
				/>
				{isClassNameError && (
					<FormErrorMessage>Không hợp lệ</FormErrorMessage>
				)}
			</FormControl>
		</form>
	);
};

export default Form;
