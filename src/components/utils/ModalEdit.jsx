import {
	Modal,
	ModalOverlay,
	ModalContent,
	ModalHeader,
	ModalFooter,
	ModalBody,
	ModalCloseButton,
	useDisclosure,
} from "@chakra-ui/react";
import { Button } from "@chakra-ui/react";
import Form from "./Form";

const ModalEdit = ({ student, setNeededLoad, neededLoad }) => {
	const { isOpen, onClose, onOpen } = useDisclosure();

	return (
		<>
			<Button onClick={onOpen}>Edit</Button>
			<Modal isOpen={isOpen} onClose={onClose}>
				<ModalOverlay />
				<ModalContent>
					<ModalHeader>
						<ModalCloseButton />
					</ModalHeader>
					<ModalBody>
						<Form
							onClose={onClose}
							setNeededLoad={setNeededLoad}
							neededLoad={neededLoad}
							student={student}
							method="update"
						/>
					</ModalBody>
					<ModalFooter>
						<Button type="submit" form="newStudent">
							Cập nhật
						</Button>
					</ModalFooter>
				</ModalContent>
			</Modal>
		</>
	);
};

export default ModalEdit;
