class Student {
    constructor(id, name, gender, className) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.className = className;
    }
}
export default Student;